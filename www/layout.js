var Layout = {
    forceFixedViewport: function(successCallback, errorCallback) {
        cordova.exec(successCallback || function() {},
                     errorCallback || function() {},
                     'FixedWidth', 'forceFixedViewport', []);
    }
};
module.exports = Layout;