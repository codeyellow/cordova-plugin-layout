/**
 * A plugin for Apache Cordova (Phonegap) to force a fixed width
 * viewport.
 *
 * Copyright (c) 2014, Code Yellow B.V.
 *
 * Heavily based on Jean-Baptiste Simillon's writeup
 * http://www.microbasic.net/2013/11/how-to-fixed-page-width-on-android-webview-or-cordovaphonegap/
 */
package nl.codeyellow.plugin;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.Runnable;
import android.view.Display;
import android.view.WindowManager;
import android.content.Context;
import android.webkit.WebSettings;

public class FixedWidthPlugin extends CordovaPlugin {
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext)
		throws JSONException
	{
		final CallbackContext ctx = callbackContext; // Silly Java

		if (action.equals("forceFixedViewport")) {
			cordova.getActivity().runOnUiThread(new Runnable() { // God this is retarded
				@Override
				public void run() {
					forceFixedViewport();
					ctx.success((String)null);
				}
			});
			return true;
		} else {
			callbackContext.error("Unknown action: "+action);
			return false;
		}
	}

	private int getScale()
    {
        Display display = ((WindowManager) cordova.getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width) / 800d;
        val = val * 100d;
        return val.intValue();
    }

    private void forceFixedViewport()
    {
        WebSettings settings = webView.getSettings();

        settings.setLoadWithOverviewMode(false);
        // Activating viewport on Android 2.x will deactivate stretching
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB)
        {
            // Set default zoom to unzoom, no setting this will sometimes trigger zoom 100% on some phone (like double tap)
            // It seems to glitch on Android 2.x, a white screen will appear after enabling this option
            //settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
            // Force using viewport html statement, sadly it activates double tap to zoom
            settings.setUseWideViewPort(true);
        }
        // Try not to use default zoom (useful ?)
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        // Set scale on devices that supports it
        webView.setPadding(0, 0, 0, 0);

        //Enable DOM storage, and tell Android where to save the Database
        //settings.setDatabasePath("/data/data/" + this.getPackageName() + "/databases/");

        int percentScale = getScale();
        webView.setInitialScale(percentScale);
    }

    private void defaultFixedViewport()
    {
        WebSettings settings = webView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
    }
}
